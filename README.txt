PagBrasil integration for the Drupal Commerce payment and checkout system. Supports online payments using PagBrasil API.

About PagBrasil
---------------

PagBrasil is the premier online payment processing service in Brazil designed for international merchants, e-commerce platforms and payment service providers. With PagBrasil you can sell cross border and receive the funds in US Dollars or Euros in your country without having a local company in Brazil* or you can also sell locally and be paid in Brazilian Real. Gain access to the broadest set of local payment methods, which may immediately multiply your sales within Brazil. We are experts in the entire sales and buying process with more than 15 years of online payment processing experience in Brazil.

Installation
------------

Download and enable the module.
Go to /admin/commerce/config/payment-methods and enable "PagBrasil"
Click on "PagBrasil" then click edit for "Enable payment method: PagBrasil" and under actions
Fill in the following fields: Secret Phrase, API Token, Payment Mode (test or live), Limit accepted credit cards to the following types (select which cards you will accept), Default currency
Click Save

Testing/Sandbox mode
--------------------

To test a credit or debit card charge, please use the following details:
Brand: V
Number: 4984123412341234
CVV: 123
Expiration: 12/19

You may use whatever data you wish to complete the other sections
(e.g.credit card holder, installments, etc).

Charges to the above credit card will always be authorized.
To test a failed payment, you just need to change the CVV or
expiration date.

You may find an example of the POST to generate credit card
orders at https://www.pagbrasil.com/pagbrasil/test_ordercc.mv

You may find an example of the POST to generate debit card
orders at https://www.pagbrasil.com/pagbrasil/test_orderdc.mv

You may find an example of the POST to generate boletos bancarios at
https://www.pagbrasil.com/pagbrasil/test_order.mv